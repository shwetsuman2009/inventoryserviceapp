package com.classpath.inventoryservice.conroller;

import com.classpath.inventoryservice.model.Item;
import com.classpath.inventoryservice.service.SimpleSourceBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/inventory")
public class InventoryController {

    private static int counter = 10000;

    @Autowired
    private SimpleSourceBean sourceBean;

    @GetMapping
    public int fetchItemsCount(){
        return counter;
    }

    @PostMapping
    public ResponseEntity<Integer> updateQty(){
        --counter;
        return ResponseEntity.ok(counter);
    }

    @PostMapping("/items")
    public Item postItem(@RequestBody Item item){
        this.sourceBean.publishItem(item);
        return item;
    }

}